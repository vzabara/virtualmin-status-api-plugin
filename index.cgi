#!/usr/bin/perl
use strict;
use warnings;

our (%text, %in);
our $module_name;

require './virtualmin-status-api-lib.pl';
&ReadParse();

my $d = &virtual_server::get_domain($in{'dom'});
$d || &error($text{'domain_egone'});
&virtual_server::can_edit_domain($d) || &error($text{'edit_ecannot'});

my $ddesc = &virtual_server::domain_in($d);

&ui_print_header($ddesc, $text{'index_desc'}, '', undef, undef, 1);

print &ui_form_start('save.cgi', 'post');
print &ui_hidden('dom', $in{'dom'});
print &ui_table_start($text{'edit_title'}, undef, 2);

print &ui_table_row($text{'edit_port'},
    &ui_textbox('port', $d->{'status_port'}));

my $cfile = "/usr/local/bin/compoza.net.info/config.ini";
my $lref = &read_file_lines($cfile);
my $title;
my $url;
my $token;
foreach my $l (@$lref) {
    if ($l =~ /^title\s?=\s?(.*)/) {
        $title = $1;
    }
    elsif ($l =~ /^url\s?=\s?(.*)/) {
        $url = $1;
    }
    elsif ($l =~ /^token\s?=\s?(.*)/) {
        $token = $1;
    }
}
print &ui_table_row($text{'view_token'},
    &ui_textarea('token', $token, 5, undef, undef, 1));
my $qr_text = urlize("compozanet://server/{\"ID\":\"$d->{'firebase_id'}\",\"title\":\"$title\",\"url\":\"$url\",\"token\":\"$token\"}");
print &ui_table_row($text{'view_settings'},
    "<img src='qr.cgi?text=$qr_text'>");

print &ui_table_end();
print &ui_form_end([ [ undef, $text{'save'} ] ]);

