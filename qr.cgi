#!/usr/bin/perl

use v5.12;
use CGI;
use Imager::QRCode;

my $q = CGI->new;
my $text = $q->param('text');
if (defined $text) {
    my $qrcode = Imager::QRCode->new(
        size          => 5,
        margin        => 1,
        version       => 1,
        level         => 'L',
        casesensitive => 1,
        lightcolor    => Imager::Color->new(255, 255, 255),
        darkcolor     => Imager::Color->new(0, 0, 0),
    );
    my $img = $qrcode->plot($text);
    print $q->header('image/gif');
    $img->write(fh => \*STDOUT, type => 'gif')
        or die $img->errstr;
}
