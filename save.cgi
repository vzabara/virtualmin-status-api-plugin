#!/usr/bin/perl
use strict;
use warnings;
our (%in, %text, %config);

require './virtualmin-status-api-lib.pl';
require './virtual_feature.pl';
&ReadParse();
$in{'dom'} || &error($text{'index_edom'});
my $d = &virtual_server::get_domain($in{'dom'});
$d || &error($text{'index_edom2'});
my $ddesc = &virtual_server::domain_in($d);

# Make sure something was done
&error_setup($text{'save_err'});
$d->{'status_port'} ||= $config{'port'};
if ($in{'port'} eq $d->{'status_port'}) {
	&error($text{'save_enone'});
}

&ui_print_header($ddesc, $text{'save_title'}, "");

&$virtual_server::first_print($text{'save_script'});
# reconfigure systemd script
&system_logged("sed -i 's/--port $d->{'status_port'}/--port $in{'port'}/g' /etc/systemd/system/status-info.service");
my $protocol = &virtual_server::domain_has_ssl($d) ? 'https' : 'http';
&system_logged("sed -i 's/$protocol:\/\/$d->{'dom'}:$d->{'status_port'}/$protocol:\/\/$d->{'dom'}:$in{'port'}/g' /usr/local/bin/compoza.net.info/config.ini");
&system_logged('systemctl daemon-reload');
&system_logged("systemctl restart status-info.service");
&$virtual_server::second_print($virtual_server::text{'setup_done'});
&$virtual_server::first_print($text{'save_firebase'});
my $res = &system_logged("/usr/local/bin/compoza.net.info/update.py --doc=$d->{'firebase_id'} --url=$protocol:\/\/$d->{'dom'}:$in{'port'}");
if ($res) {
    &$virtual_server::second_print($text{'error_firebase'});
} else {
    &$virtual_server::second_print($virtual_server::text{'setup_done'});
}

# Update the domain
&$virtual_server::first_print($text{'save_domain'});
$d->{'status_port'} = $in{'port'};
&virtual_server::save_domain($d);
&$virtual_server::second_print($virtual_server::text{'setup_done'});

&virtual_server::run_post_actions();
&webmin_log("modify", "status_port", undef, { 'dom' => $d->{'dom'} });

&ui_print_footer("index.cgi?dom=$in{'dom'}", $text{'index_desc'});
