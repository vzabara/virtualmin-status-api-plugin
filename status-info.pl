#!/usr/bin/perl

=head1 status-info.pl

Show general info about this system in JSON format

This command is useful when debugging or configuring a system that you
don't know much about, to fetch general information about Webmin, Virtualmin,
IP usage and installed programs.

=cut

use WebminCore;
&init_config();
&foreign_require("virtual-server", "virtual-server-lib.pl");

if (!$module_name) {
    $main::no_acl_check++;
    $ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
    $ENV{'WEBMIN_VAR'} ||= "/var/webmin";
    if ($0 =~ /^(.*)\/[^\/]+$/) {
	chdir($pwd = $1);
	}
    else {
	chop($pwd = `pwd`);
	}
    $0 = "$pwd/status-info.pl";
    $< == 0 || die "status-info.pl must be run as root";
    }

while(@ARGV > 0) {
    local $a = shift(@ARGV);
    if ($a eq "--help") {
	&usage();
	}
    }

use Socket;
$hostname = &virtual_server::get_system_hostname();
$ip = gethostbyname($hostname);
$ip_address = inet_ntoa($ip);

$system_date = `date`;
$system_date =~ s/\s$//;
$up_time = `uptime -p`;
$up_time =~ s/\s$//;

$info = &virtual_server::get_collected_info();
my $updates = 0;
if (@{$info->{'poss'}}) {
    $updates = @{$info->{'poss'}};
}
my $kernel = "";
if ($info->{'kernel'}) {
    $kernel = $info->{'kernel'}->{'os'}.' '.$info->{'kernel'}->{'version'}.' '.$info->{'kernel'}->{'arch'};
}
$info->{'host'} = { 'hostname', $hostname,
	    'date' => $system_date,
	    'uptime' => substr($up_time, 3),
	    'ip' => $ip_address,
	    'os' => $gconfig{'real_os_type'}.' '.$gconfig{'real_os_version'},
	    'kernel' => $kernel,
	    'updates' => $updates,
	  };
$info->{'status'} = $info->{'startstop'};
foreach $s (@{$info->{'status'}}) {
    delete($s->{'desc'});
    delete($s->{'longdesc'});
    delete($s->{'links'});
    delete($s->{'restartdesc'});
    delete($s->{'startdesc'});
    delete($s->{'stopdesc'});
    }
delete($info->{'startstop'});
delete($info->{'quota'});
delete($info->{'inst'}) if (!@{$info->{'inst'}});
delete($info->{'poss'}) if (!@{$info->{'poss'}});
delete($info->{'vposs'}) if (!@{$info->{'vposs'}});
delete($info->{'fextra'});
delete($info->{'fhide'});
delete($info->{'fmax'});

eval "use JSON::PP";
return "Missing Perl module JSON::PP" if ($@);
my $coder = JSON::PP->new;
my $enc = $coder->encode($info);
print $enc;


sub usage
{
print "$_[0]\n\n" if ($_[0]);
print "Displays information about this Virtualmin system in JSON format.\n";
print "\n";
print "virtualmin status-info\n";
exit(1);
}

