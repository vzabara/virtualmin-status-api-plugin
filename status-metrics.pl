#!/usr/bin/perl

=head1 status-metrics.pl

Show metrics info about this system in JSON format

This command is useful when debugging or configuring a system that you
don't know much about, to fetch general information about Webmin, Virtualmin,
IP usage and installed programs.

=cut

use WebminCore;
&init_config();
&foreign_require("virtual-server", "virtual-server-lib.pl");
&foreign_require("system-status", "system-status-lib.pl");

if (!$module_name) {
    $main::no_acl_check++;
    $ENV{'WEBMIN_CONFIG'} ||= "/etc/webmin";
    $ENV{'WEBMIN_VAR'} ||= "/var/webmin";
    if ($0 =~ /^(.*)\/[^\/]+$/) {
	    chdir($pwd = $1);
	}
    else {
	    chop($pwd = `pwd`);
	}
    $0 = "$pwd/status-metrics.pl";
    $< == 0 || die "status-metrics.pl must be run as root";
}

while(@ARGV > 0) {
    local $a = shift(@ARGV);
    if ($a eq "--help") {
	    &usage();
	}
}

our %text = load_language($current_theme);

my $data;
$data->{'os_type'} = $gconfig{'os_type'};

&foreign_require("proc");

# CPU stats
my @cpuinfo  = defined(&proc::get_cpu_info)     ? proc::get_cpu_info()     : ();
my @cpuusage = defined(&proc::get_cpu_io_usage) ? proc::get_cpu_io_usage() : ();
if (@cpuinfo && @cpuusage) {

    # CPU load
    my $cpu = int($cpuusage[0] + $cpuusage[1] + $cpuusage[3]);
    $data->{'cpu'} = [$cpu, text('body_load', ($cpuinfo[0], $cpuinfo[1], $cpuinfo[2]))];
    $data->{'cpu_model'} = $cpuinfo[4] || "";
    if ($data->{'cpu_model'} eq "") {
        if ($gconfig{'os_type'} =~ /-linux$/) {
            $data->{'cpu_model'} = &lscpu_arm();
        }
    }
    if ($data->{'cpu_model'} eq "") {
        if (&has_command('dpkg')) {
            $data->{'cpu_model'} = `dpkg --print-architecture`;
        }
    }
    chomp($data->{'cpu_model'});

    # Disk I/O
    my $in  = $cpuusage[5];
    my $out = $cpuusage[6];
    if ($in && $out || $in eq '0' || $out eq '0') {
        $data->{'io'} = [$in, $out];
    }
}

# Memory stats
my @memory = defined(&proc::get_memory_info) ? proc::get_memory_info() : ();
if (@memory) {
    $data->{'mem'}  = [];
    $data->{'virt'} = [];

    if (@memory && $memory[0] && $memory[0] > 0) {
        my $mem = (100 - int(($memory[1] / $memory[0]) * 100));
        $data->{'mem'} = [$mem,
                        text(($memory[4] ? 'body_used_cached_total' : 'body_used'),
                             nice_size($memory[0] * 1024),
                             nice_size(($memory[0] - $memory[1]) * 1024),
                             ($memory[4] ? nice_size($memory[4] * 1024) : undef)
                        )];

    }
    if (@memory && $memory[2] && $memory[2] > 0) {
        my $virt = (100 - int(($memory[3] / $memory[2]) * 100));
        $data->{'virt'} = [$virt,
                         text('body_used',
                              nice_size(($memory[2]) * 1024),
                              nice_size(($memory[2] - $memory[3]) * 1024)
                         )];
    }
}

# Number of running processes
my @processes = proc::list_processes();
my $proc      = scalar(@processes);
$data->{'proc'} = $proc;

# Disk space
&foreign_require("mount");

my @disk_space = defined(&mount::local_disk_space) ? mount::local_disk_space() : ();
if (@disk_space) {
    $data->{'disk'} = [];

    if (@disk_space && $disk_space[0] && $disk_space[0] > 0) {
        my $disk = int(($disk_space[0] - $disk_space[1]) / $disk_space[0] * 100);
        $data->{'disk'} = [$disk,
                         text('body_used_and_free',
                              nice_size($disk_space[0]),
                              nice_size($disk_space[1]),
                              nice_size($disk_space[0] - $disk_space[1])
                         )];
    }
}

eval "use JSON::PP";
return "Missing Perl module JSON::PP" if ($@);
my $coder = JSON::PP->new;
print $coder->encode($data);


sub usage
{
    print "$_[0]\n\n" if ($_[0]);
    print "Displays metrics information about this Virtualmin system in JSON format.\n";
    print "\n";
    print "virtualmin status-metrics\n";
    exit(1);
}

sub lscpu_arm
{
    my @arm_part = (
        { code => "0x810", title => "ARM810" },
        { code => "0x920", title => "ARM920" },
        { code => "0x922", title => "ARM922" },
        { code => "0x926", title => "ARM926" },
        { code => "0x940", title => "ARM940" },
        { code => "0x946", title => "ARM946" },
        { code => "0x966", title => "ARM966" },
        { code => "0xa20", title => "ARM1020" },
        { code => "0xa22", title => "ARM1022" },
        { code => "0xa26", title => "ARM1026" },
        { code => "0xb02", title => "ARM11 MPCore" },
        { code => "0xb36", title => "ARM1136" },
        { code => "0xb56", title => "ARM1156" },
        { code => "0xb76", title => "ARM1176" },
        { code => "0xc05", title => "Cortex-A5" },
        { code => "0xc07", title => "Cortex-A7" },
        { code => "0xc08", title => "Cortex-A8" },
        { code => "0xc09", title => "Cortex-A9" },
        { code => "0xc0d", title => "Cortex-A17" }, # Originally A12
        { code => "0xc0f", title => "Cortex-A15" },
        { code => "0xc0e", title => "Cortex-A17" },
        { code => "0xc14", title => "Cortex-R4" },
        { code => "0xc15", title => "Cortex-R5" },
        { code => "0xc17", title => "Cortex-R7" },
        { code => "0xc18", title => "Cortex-R8" },
        { code => "0xc20", title => "Cortex-M0" },
        { code => "0xc21", title => "Cortex-M1" },
        { code => "0xc23", title => "Cortex-M3" },
        { code => "0xc24", title => "Cortex-M4" },
        { code => "0xc27", title => "Cortex-M7" },
        { code => "0xc60", title => "Cortex-M0+" },
        { code => "0xd01", title => "Cortex-A32" },
        { code => "0xd03", title => "Cortex-A53" },
        { code => "0xd04", title => "Cortex-A35" },
        { code => "0xd05", title => "Cortex-A55" },
        { code => "0xd06", title => "Cortex-A65" },
        { code => "0xd07", title => "Cortex-A57" },
        { code => "0xd08", title => "Cortex-A72" },
        { code => "0xd09", title => "Cortex-A73" },
        { code => "0xd0a", title => "Cortex-A75" },
        { code => "0xd0b", title => "Cortex-A76" },
        { code => "0xd0c", title => "Neoverse-N1" },
        { code => "0xd0d", title => "Cortex-A77" },
        { code => "0xd0e", title => "Cortex-A76AE" },
        { code => "0xd13", title => "Cortex-R52" },
        { code => "0xd20", title => "Cortex-M23" },
        { code => "0xd21", title => "Cortex-M33" },
        { code => "0xd41", title => "Cortex-A78" },
        { code => "0xd42", title => "Cortex-A78AE" },
        { code => "0xd4a", title => "Neoverse-E1" },
        { code => "0xd4b", title => "Cortex-A78C" }
    );
    my @implementer = (
        { code => "0x41",  title => "ARM" },
        { code => "0x42",  title => "Broadcom" },
        { code => "0x43",  title => "Cavium" },
        { code => "0x44",  title => "DEC" },
        { code => "0x46",  title => "FUJITSU" },
        { code => "0x48",  title => "HiSilicon" },
        { code => "0x4e",  title => "Nvidia" },
        { code => "0x50",  title => "APM" },
        { code => "0x51",  title => "Qualcomm" },
        { code => "0x53",  title => "Samsung" },
        { code => "0x56",  title => "Marvell" },
        { code => "0x61",  title => "Apple" },
        { code => "0x66",  title => "Faraday" },
        { code => "0x69",  title => "Intel" }
    );
    my ($vendor_code, $model_code, $vendor, $model);
    open(FH, '<', "/proc/cpuinfo") or return "";
    while(<FH>) {
        if ($_ =~ /CPU implementer\s+?:\s+?(.*)/) {
            $vendor_code = $1;
        } elsif ($_ =~ /CPU part\s+?:\s+?(.*)/) {
            $model_code = $1;
        }
    }
    close(FH);
    if ($vendor_code) {
        foreach my $h (@implementer) {
            if ($h->{'code'} eq $vendor_code) {
                $vendor = $h->{'title'};
            }
        }
    }
    if ($model_code) {
        foreach my $h (@arm_part) {
            if ($h->{'code'} eq $model_code) {
                $model = $h->{'title'};
            }
        }
    }
    return "$vendor $model";
}
