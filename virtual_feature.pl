use strict;
use warnings;
our (%text, %config);
our $module_name;

do 'virtualmin-status-api-lib.pl';

sub feature_name
{
    return 'Status API';
}

sub feature_losing
{
    return 'Compoza.NET Status API Server';
}

sub feature_label
{
    my ($edit) = @_;
    return $edit ? 'Status API' : 'Status API?';
}

sub feature_check
{
    &has_command('openssl') || return 'openssl is needed';
    &has_command('sqlite3') || return 'sqlite3 is needed';
    &has_command('python') || return 'python is needed';
    &has_command('pip') || return 'pip is needed';
    return undef;
}

sub feature_suitable
{
    my ($d, $alias, $super) = @_;
    return $alias || $super ? 0 : 1;          # not for alias domains
}

sub feature_depends
{
    return $_[0]->{'web'} ? undef : 'Enable website';
}

sub feature_clash
{
    !-d '/usr/local/bin/compoza.net.info' || return 'Already installed';
    return undef;
}

sub feature_setup
{
    my ($d) = shift;
    my $out;
    &$virtual_server::first_print('Status API setup ..');
    $d->{'status_port'} ||= $config{'port'};
    my $res = &install_scripts($d);
    if ($res) {
        &$virtual_server::second_print(".. failed");
        return 1;
    }
    &$virtual_server::second_print('.. done');
    &$virtual_server::first_print('Registering server at Firebase ..');
    $res = `/usr/local/bin/compoza.net.info/register.py`;
    chomp($res);
    if ($res) {
        $d->{'firebase_id'} = $res;
        &$virtual_server::second_print('.. done');
        &$virtual_server::first_print('System scripts setup ..');
        &create_system_scripts($d);
        &$virtual_server::second_print('.. done');
        return 1;
    }
    &$virtual_server::second_print(".. failed");
    return 1;
}

sub feature_delete
{
    my ($d) = @_;
    &$virtual_server::first_print('Status API deactivation ..');
    &uninstall_scripts($d);
    &$virtual_server::second_print('.. done');
}

sub feature_links
{
    my ($d) = @_;
    return ( { 'mod' => $module_name,
        'desc' => 'Status API',
        'page' => "index.cgi?dom=$d->{'id'}",
        'cat' => 'services',
    } );
}

#sub feature_webmin
#{
#    my ($d, $all) = @_;
#    my @fdoms = grep { $_->{$module_name} } @$all;
#    if (@fdoms) {
#        return ( [ $module_name, { 'doms' => join(" ", @fdoms) } ] );
#    }
#    else {
#        return ( );
#    }
#}

sub feature_backup
{
    my ($d) = @_;
    &$virtual_server::first_print('Copying system scripts ..');
    system("cp -p /etc/systemd/system/status-info.service".
        " $d->{'home'}/etc/status-info.service");
    &$virtual_server::second_print('.. done');
    return 1;
}

sub feature_restore
{
    my ($d) = @_;
    &$virtual_server::first_print('Restore system scripts ..');
    system("cp -p $d->{'home'}/etc/status-info.service".
        " /etc/systemd/system/status-info.service");
    &$virtual_server::second_print('.. done');
    return 1;
}

1;
