use strict;
use warnings;
our (%text, %in, %config);
our $module_name;

use WebminCore;
&init_config();
&foreign_require("virtual-server", "virtual-server-lib.pl");

sub install_scripts
{
    my ($d) = @_;

    my $install_dir = "/usr/local/bin/compoza.net.info";
    &system_logged("git clone git\@bitbucket.org:vzabara/compoza.net-info-api.git $install_dir");
    -r "$install_dir/config.ini" || return 1;

    my $title = $d->{'owner'};
    if ($title eq "") {
        $title = $d->{'dom'};
    }
    my $port = $d->{'port'};
    if ($port eq "") {
        $port = 5000;
    }
    my $protocol = &virtual_server::domain_has_ssl($d) ? 'https' : 'http';
    my $cfile = "$install_dir/config.ini";
    my $lref = &read_file_lines($cfile);
    foreach my $l (@$lref) {
        if ($l =~ /^title\s?=/) {
            $l = "title = $title";
        }
        if ($l =~ /^url\s?=/) {
            $l = "url = $protocol://$d->{'dom'}:$port";
        }
        if ($l =~ /^token\s?=/) {
            $l = "token = " . `openssl rand -hex 80`;
        }
    }
    &flush_file_lines($cfile);
    &system_logged("ln -s $install_dir/compozanet-info-firebase-adminsdk-42dgn-e107bb3266.json".
        " $install_dir/serviceAccountKey.json");
    return 0;
}

sub uninstall_scripts
{
    my ($d) = @_;
    &system_logged("rm -rf /usr/local/bin/compoza.net.info");
    &system_logged("systemctl stop status-info.service");
    &system_logged("systemctl disable status-info.service");
    unlink("/etc/systemd/system/status-info.service");

    &foreign_require("cron");
    my $job1;
    my $cron_cmd1 = "/usr/local/bin/compoza.net.info/check-services.py --server-id=$d->{'firebase_id'} >> /usr/local/bin/compoza.net.info/check-services-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job1 = $j if ($j->{'command'} eq $cron_cmd1);
    }
    if ($job1) {
        &lock_file(&cron::cron_file($job1));
        &cron::delete_cron_job($job1);
        &unlock_file(&cron::cron_file($job1));
    }
    my $job2;
    my $cron_cmd2 = "/usr/local/bin/compoza.net.info/check-updates.py --server-id=$d->{'firebase_id'} >> /usr/local/bin/compoza.net.info/check-updates-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job2 = $j if ($j->{'command'} eq $cron_cmd2);
    }
    if ($job2) {
        &lock_file(&cron::cron_file($job2));
        &cron::delete_cron_job($job2);
        &unlock_file(&cron::cron_file($job2));
    }
}

sub create_system_scripts
{
    my ($d) = @_;
    &system_logged('pip install firebase_admin fastapi configparser uvicorn[standard]');
    my $uvicorn = `which uvicorn`;
    chomp($uvicorn);
    my $cert = &virtual_server::domain_has_ssl($d) ?
        "--ssl-keyfile=$d->{home}/ssl.key --ssl-certfile=$d->{home}/ssl.cert --ssl-ca-certs=$d->{home}/ssl.ca" :
        "";
    my $str = <<END_SCRIPT;
[Unit]
Description=Status Info Systemd Service
After=network.target
StartLimitIntervalSec=0

[Service]
Type=simple
ExecStart=$uvicorn main:app --host 0.0.0.0 --port $d->{'status_port'} $cert
User=root
Group=root
WorkingDirectory=/usr/local/bin/compoza.net.info
ExecReload=/bin/kill -s HUP \$MAINPID
KillMode=mixed
TimeoutStopSec=5
PrivateTmp=true
RestartSec=10
Restart=always

[Install]
WantedBy=multi-user.target
END_SCRIPT

    open(FH, '>', "/etc/systemd/system/status-info.service") or die $!;
    print FH $str;
    close(FH);

    &system_logged('systemctl daemon-reload');
    &system_logged("systemctl start status-info.service");
    &system_logged("systemctl enable status-info.service");

    &foreign_require("cron");
    my $job1;
    my $cron_cmd1 = "/usr/local/bin/compoza.net.info/check-services.py --server-id=$d->{'firebase_id'} >> /usr/local/bin/compoza.net.info/check-services-log.txt 2>&1";
    foreach my $j (&cron::list_cron_jobs()) {
        $job1 = $j if ($j->{'command'} eq $cron_cmd1);
    }
    if (!$job1) {
        my $njob = { 'user' => 'root', 'active' => 1,
            'mins' => '*/5', 'hours' => '*', 'days' => '*',
            'months' => '*', 'weekdays' => '*',
            'command' => $cron_cmd1 };
        &lock_file(&cron::cron_file($njob));
        &cron::create_cron_job($njob);
        &unlock_file(&cron::cron_file($njob));
    }
    my $cron_cmd2 = "/usr/local/bin/compoza.net.info/check-updates.py --server-id=$d->{'firebase_id'} >> /usr/local/bin/compoza.net.info/check-updates-log.txt 2>&1";
    my $job2;
    foreach my $j (&cron::list_cron_jobs()) {
        $job2 = $j if ($j->{'command'} eq $cron_cmd2);
    }
    if (!$job2) {
        my $njob = { 'user' => 'root', 'active' => 1,
            'mins' => '15', 'hours' => '*/6', 'days' => '*',
            'months' => '*', 'weekdays' => '*',
            'command' => $cron_cmd2 };
        &lock_file(&cron::cron_file($njob));
        &cron::create_cron_job($njob);
        &unlock_file(&cron::cron_file($njob));
    }
}

1;
